# webrit-test-vue

> Frontend разработка теста. Используется - <b>Webpack</b>, <b>PUG</b>, <b>SCSS</b>, <b>Vue</b>.

## Для запуска проекта

1. Склонировать данную репозиторию
1. Перейти в папку с проектом
1. В терминали(консоли) прописать npm install
1. - Для разработки - npm run dev
   - Для продакшен - npm run build

## Контакты

Автор: Гинятов Али – [@vk](https://vk.com/spartanec4) – webritinfo@gmail.com
