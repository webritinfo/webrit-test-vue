import './js/common'
import './assets/scss/main.scss'

window.Vue = require('vue')


Vue.component('test', require('./vue/test.vue').default)

const app = new Vue({
	el: '#app'
})