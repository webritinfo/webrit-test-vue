export const questions = [{
		id: '1',
		question: 'Какие сервисы компании могут стать мишенью для DDoS-атак?',
		rightAnswer: 4,
		img: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_1.0,f_auto,fl_lossy,q_auto:good,w_800/v1/polls/ogivesrwoskjvbxvc4re',
		answers: {
			1: 'Общедоступные сайты и порталы для клиентов',
			2: 'Серверы почты и сервисы сообщений',
			3: 'Файл-серверы и сервисы, обеспечивающие транзакции',
			4: 'Все вышеперечисленное',
		}
	},
	{
		id: '2',
		question: 'Как вы думаете, насколько долгой была самая продолжительная DDoS-атака?',
		description: 'Подзаголовок какой то интересный',
		rightAnswer: 3,
		img: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_1.0,f_auto,fl_lossy,q_auto:good,w_800/v1/polls/f8x7opxqm0xwo8ctuu4w',
		answers: {
			1: '24 часа (день)',
			2: '96 часов (три дня)',
			3: '288 часов (12 дней)',
			4: '576 часов (24 дня)',
		}
	},
	{
		id: '3',
		question: 'Какая была пропускная способность у самой мощной DDoS-атаки?',
		rightAnswer: 4,
		img: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_1.0,f_auto,fl_lossy,q_auto:good,w_800/v1/polls/tzluovcyd6spqzpslsxd',
		answers: {
			1: 'Около 500 Мбит/с',
			2: 'Около 1 Гбит/с',
			3: 'Около 500 Гбит/с',
			4: 'Более 1 Тбит/с',
		}
	},
	{
		id: '4',
		question: 'Как по-вашему, сколько стоит заказать DDoS-атаку?',
		rightAnswer: 1,
		img: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_1.0,f_auto,fl_lossy,q_auto:good,w_800/v1/polls/vgocpwcujlpiqydw4uzl',
		answers: {
			1: '$50 в день',
			2: '$50 в час',
			3: '$50 в секунду',
			4: '$50 за наносекунду',
		}
	},
	{
		id: '5',
		question: 'В 2013 году была зафиксирована DDoS-атака с рекордной для тех времен пропускной способностью, организованная одним британцем. Как вы думаете, сколько ему тогда было лет?',
		rightAnswer: 2,
		img: 'https://opinionstage-res.cloudinary.com/image/upload/c_lfill,dpr_1.0,f_auto,fl_lossy,q_auto:good,w_800/v1/polls/tfmnoc4bo6n3ss4abhnc',
		answers: {
			1: '13',
			2: '16',
			3: '20',
			4: '87',
		}
	}
]